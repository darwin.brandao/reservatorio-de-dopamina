# Lista de fontes de artigos científicos usados por Eslen Delanogare

Abaixo, temos uma lista de fontes de artigos científicos mencionadas pelo Eslen Delanogare no episódio ["006 - ONDE BUSCO ARTIGOS CIENTÍFICOS"](https://treinamentos.eslendelanogare.com.br/55258-aulas-rd-2022/1334653-006-onde-busco-artigo-cientifico) na plataforma do [Reservatório de Dopamina](https://treinamentos.eslendelanogare.com.br/).

O Reservatório de Dopamina é uma plataforma paga e comercial de conteúdos sobre neurociências, psicologia, comportamento humano, estilo de vida, dentre outros assuntos relacionados. Vale muito a pena assinar, o Eslen Delanogare tem uma didática incrível e muito conteúdo de qualidade.

Este repositório git não está de forma alguma relacionado ao Reservatório de Dopamina, exceto pelo fato de que sou aluno assinante da plataforma.

- Chronobiology International

	URL: https://www.tandfonline.com/toc/icbi20/current  
	RSS: https://www.tandfonline.com/feed/rss/icbi20  

- Cell Reports

	URL: https://www.cell.com/cell-reports/home  
	RSS: https://www.cell.com/cell-reports/current.rss  
	RSS: https://www.cell.com/cell-reports/inpress.rss  

- Clinical Psychology Review

	URL: https://www.sciencedirect.com/journal/clinical-psychology-review  
	RSS: https://rss.sciencedirect.com/publication/science/02727358  

- PNAS

	URL: https://www.pnas.org/  
	URL: https://www.pnas.org/about/rss		(All RSS Feeds, they have a lot of them)  
	RSS: https://www.pnas.org/action/showFeed?type=searchTopic&taxonomyCode=topic&tagCode=neuro  
	RSS: https://www.pnas.org/action/showFeed?type=searchTopic&taxonomyCode=topic&tagCodeOr=psych-soc&tagCodeOr=psych-bio  
	RSS: https://www.pnas.org/action/showFeed?type=etoc&feed=rss&jc=PNAS  
	RSS: https://www.pnas.org/action/showFeed?type=searchTopic&taxonomyCode=type&tagCode=twip  
	RSS: https://pnas-science-sessions-podcast.libsyn.com/rss  

- JNeurosci (Journal of Neuroscience)

	URL: https://www.jneurosci.org  
	URL: https://www.jneurosci.org/content/rss		(Al RSS Feeds)  
	RSS: https://www.jneurosci.org/rss/current.xml  
	RSS: https://www.jneurosci.org/rss/recent.xml  
	RSS: https://www.jneurosci.org/rss/mfr.xml  
	RSS: https://www.jneurosci.org/rss/mfc.xml  

- JAMA Psychiatry

	URL: https://jamanetwork.com/journals/jamapsychiatry  
	RSS: https://jamanetwork.com/rss/site_14/70.xml  

- eNeuro

	URL: https://www.eneuro.org/  
	URL: https://www.eneuro.org/content/rss		(All RSS Feeds)  
	RSS: https://www.eneuro.org/rss/current.xml  
	RSS: https://www.eneuro.org/rss/recent.xml  
	RSS: https://www.eneuro.org/rss/mfr.xml  
	RSS: https://www.eneuro.org/rss/mfc.xml  

- Bipolar Disorders

	URL: https://onlinelibrary.wiley.com/journal/13995618  
	RSS: https://onlinelibrary.wiley.com/feed/13995618/most-recent  
	RSS: https://onlinelibrary.wiley.com/feed/13995618/most-cited  

- International Journal of Bipolar Disorders

	URL: https://journalbipolardisorders.springeropen.com/  
	RSS: https://journalbipolardisorders.springeropen.com/articles/most-recent/rss.xml  

- Translational Psychiatry

	URL: https://www.nature.com/tp/  
	RSS: https://www.nature.com/tp.rss  

- Molecular Psychiatry

	URL: https://www.nature.com/mp/  
	RSS: https://www.nature.com/mp.rss  

- Neuropsychopharmacology

	URL: https://www.nature.com/npp/  
	RSS: https://www.nature.com/npp.rss  

- Neuron

	URL: https://www.cell.com/neuron/home  
	URL: https://www.cell.com/neuron/rss		(All RSS Feeds)  
	RSS: https://www.cell.com/neuron/inpress.rss  
	RSS: https://www.cell.com/neuron/current.rss  

- Biological Psychiatry Journal

	URL: https://www.biologicalpsychiatryjournal.com/  
	RSS: https://www.biologicalpsychiatryjournal.com/current.rss  

- Dialogues in Clinical Neuroscience

	URL: https://www.tandfonline.com/journals/tdcn20  
	RSS: https://www.tandfonline.com/feed/rss/tdcn20  

- The Lancet

	URL: https://www.thelancet.com/  
	URL: https://www.thelancet.com/content/rss		(All RSS Feeds, they have a lot of them)  
	RSS: https://www.addtoany.com/add_to/feed?linkurl=http%3A%2F%2Fwww.thelancet.com%2Frssfeed%2Flanpsy_current.xml&type=feed&linkname=The%20Lancet%20Psychiatry%20Current%20Issue&linknote=  
	RSS: https://www.addtoany.com/add_to/feed?linkurl=http%3A%2F%2Fwww.thelancet.com%2Frssfeed%2Flaneur_current.xml&type=feed&linkname=The%20Lancet%20Neurology%20Current%20Issue&linknote=  

- Nature Human Behaviour

	URL: https://www.nature.com/nathumbehav/  
	RSS: https://www.nature.com/nathumbehav.rss  

- Nature Reviews Neuroscience

	URL: https://www.nature.com/nrn/  
	RSS: https://www.nature.com/nrn.rss  

- Cerebral Cortex

	URL: https://academic.oup.com/cercor  
	RSS: https://academic.oup.com/rss/site_5122/3011.xml  
	RSS: https://academic.oup.com/rss/site_5122/advanceAccess_3011.xml  
	RSS: https://academic.oup.com/rss/site_5122/OpenAccess.xml  
